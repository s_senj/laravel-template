За основу взята [данная статья](https://cutcode.dev/articles/nastroika-xdebug-3-windows-wsl-docker), но с небольшими доработками.

Вот как это выглядит у меня.

- Установка в контейнер. В Dockerfile добавляем установку xdebug и копирование конфига.
```dockerfile
RUN pecl install xdebug-3.2.2 \  
    && docker-php-ext-enable xdebug

...

COPY ./conf/php/xdebug.ini /usr/local/etc/php/conf.d/
```
В папке `conf/php` находится файл с настройками xdebug - `xdebug.ini`.
```ini
[xdebug]  
xdebug.client_host=host.docker.internal  
xdebug.start_with_request=yes  
xdebug.idekey=PHPSTORM  
xdebug.mode=debug  
xdebug.discover_client_host = true  
xdebug.client_port = 9003
```

- В `php` контейнере указываем переменные окружения
```
php:
  ...
  environment:  
    PHP_IDE_CONFIG: serverName=app-server  
    IDE_KEY: PHPSTORM
```

- Добавляем Docker в PhpStorm<br>
  ![Pasted image 20240723232413.png](./_attachments/20240723232413.png)<br>

- Добавляем интерпретатор php из нашего контейнера, здесь подробнее по шагам.

Сначала кликаем по основному пункту PHP, нажимаем на троеточие:<br>
![[Pasted image 20240723232524.png]](./_attachments/20240723232524.png)<br>
Далее выбираем вариант с докером:<br>
![[Pasted image 20240723232538.png]](./_attachments/20240723232538.png)<br>
Устанавливаем вариант Docker, в ImageName выбираем нужный образ, в котором работает проект:<br>
![[Pasted image 20240723232750.png]](./_attachments/20240723232750.png)<br>
Должно получится как показано на скриншоте ниже, мы видим версию Php и Xdebug:<br>
![[Pasted image 20240723232921.png]](./_attachments/20240723232921.png)<br>
Добавляем сервер:<br>
![[Снимок экрана 2024-07-23 в 23.30.18.png]](./_attachments/2024-07-23_23.30.18.png)<br>
Теперь нужно создать наш дебагер и добавить в него настроенный сервер:<br>
![[Pasted image 20240723233330.png]](./_attachments/20240723233330.png)<br>
![[Снимок экрана 2024-07-23 в 23.33.57.png]](./_attachments/2024-07-23_23.33.57.png)<br>

- После данных хитрых манипуляций, включайте дебаг и в путь.